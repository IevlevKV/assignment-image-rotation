#include "bmp.h"


#define DWORD_SIZE 4
#define COLOR_DEPTH 24
#define BMP_TYPE 19778
#define BMP_RESERVED 0
#define BMP_COMPRESSION 0
#define BMP_PIXEL_PER_METER 2834
#define BMP_PLANES 1
#define BMP_COLORS 0
#define BMP_HEADER_SIZE 40
#define OFF_BITS 54


static uint32_t check_padding(uint32_t width) {
     uint32_t padding = (width*(COLOR_DEPTH/8))%DWORD_SIZE;
    if (padding == 0) {return padding;
    }else {return DWORD_SIZE - padding;}
}


enum read_status read_bmp_pixel_array(FILE* in, struct image* img, struct bmp_header* header) {
    size_t height = header -> biHeight;
    size_t width = header -> biWidth;
    struct pixel* data = malloc(height * width * sizeof(struct pixel));
    img -> height = height;
    img -> width = width;
    img -> data = data;
    uint32_t padding = check_padding(width);
    for (size_t i = 0; i < height; i++){
         if(fread(data + i* width , sizeof(struct pixel), width, in) < width){
            free(data);
            return READ_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR)) {
            free(data);
            return READ_ERROR;
        }
    }
    free(header);
    return READ_OK;
}

enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    rewind(in);
    fread(header, sizeof(struct bmp_header), 1, in);
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ){
    if (!in) return READ_FILE_NULL;
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
     enum read_status header_read_status =  read_bmp_header(in, header);
    if (header_read_status != READ_OK) {
	return READ_INVALID_HEADER;}
    return read_bmp_pixel_array(in, img, header);
}




static enum write_status write_header(FILE* out, struct image const* img ){
    if (!out) return WRITE_FILE_NULL;
    uint32_t padding = check_padding(img -> width);
	struct bmp_header header;
	header.bfType = BMP_TYPE;
	header.bfileSize = OFF_BITS + padding*img->height + sizeof(struct pixel) * img->width * img->height;
	header.bfReserved = BMP_RESERVED;
    header.bOffBits = OFF_BITS;
    header.biSize = BMP_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = COLOR_DEPTH;
    header.biCompression = BMP_COMPRESSION;
    header.biSizeImage = padding*img->height + sizeof(struct pixel) * img->width * img->height;
    header.biXPelsPerMeter = BMP_PIXEL_PER_METER;
    header.biYPelsPerMeter = BMP_PIXEL_PER_METER;
    header.biClrUsed = BMP_COLORS;
    header.biClrImportant = BMP_COLORS;
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) == 1){
        return WRITE_OK;
    }else{
        return WRITE_ERROR;
    }
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if (write_header(out, img) == WRITE_OK){
        uint32_t padding = check_padding(img -> width);
	    uint32_t indent = 0; 
        for (size_t i = 0; i < img -> height; i++) {
            if (fwrite(img -> data + i * img -> width, sizeof(struct pixel), img -> width, out) != img -> width) {
                return WRITE_ERROR;
            }
            if (fwrite(&indent, 1, padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }else {
        return WRITE_ERROR;}

}
