#include "image.h"

struct image create_img(size_t width, size_t height, struct pixel* data) {
	struct image img;
	img.width = width;
	img.height = height;
	img.data = data;
	return img;
}



