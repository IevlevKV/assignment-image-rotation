#include "bmp.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    
    if (argc != 3) {
        fprintf(stderr,"Usage: ./image-transformer source-image transformed-image");
        return 0;
    }

    struct image source;
	struct image result;

    FILE* file;
    file = fopen(argv[1],"rb");
    if (file == NULL){
        fprintf(stderr, "Указатель на read file null");
        return 0;
    }

    enum read_status status_read = from_bmp(file, &source);
    if (status_read != READ_OK) {
        fprintf(stderr, "Неудолось прочитать файл");
		return 0;
	}

    if (fclose(file) != 0) {
        fprintf(stderr, "Неудолось закрыть read file");
        return 0;}

    result = rotate(source);

    file = fopen(argv[2], "wb");
	if (file == NULL) {
        fprintf(stderr, "Указатель на write file null");
        return 0;}

    enum write_status status_write = to_bmp(file, &result);
        if (status_write != WRITE_OK) {
        fprintf(stderr, "Неудолось записать в файл");    
		return 0;
	}

    if (fclose(file) != 0) {
        fprintf(stderr, "Неудолось закрыть write file");
        return 0;}
	
    free(source.data);
    free(result.data);
}
