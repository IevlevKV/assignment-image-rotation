#include "transform.h"
#include "image.h"
struct image rotate( struct image const source ){
    struct pixel* data = malloc(sizeof(struct pixel) * source.height * source.width);
    struct image img = create_img(source.height, source.width, data);
    for (uint64_t i = 0; i < source.height; i++){
        for (uint64_t j = 0; j < source.width; j++){
            img.data[j * source.height + source.height - 1 - i] = source.data[i * source.width + j];
        }
    }
    return img;
}


